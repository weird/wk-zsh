# ZSH Workshop

The shell provides an interface to the kernel via a number of abstractions
and a comfortable user environment through various conveniences.

The kernel makes things possible; the shell (in conjunction with userland)
makes things palatable.

Z shell is arguably the most complex and fully featured Linux shell
implementation. It is mostly Bash compatible.

No shell workshop is complete without a big call out to the userland. This
workshop uses the *GNU coreutils* and *util-linux* packages. It's reasonable
to assume that most Linux distributions use those two packages as the bulk
of userland - YMMV.

If you often find yourself on foreign soil, learn some of these equivalents
for:

|platform|shell|userland|
|---:|:---:|:---|
`bsd`|`sh`|`bsd`
`darwin`|`sh`|`bsd`
`linux`|`sh`, `bash`|`busybox`


## Introduction

ZSH is a modern, advanced shell for Linux and Unix operating systems.
Other than power, ZSH has excellent documentation which is helpfully divided
into sections. Although some sections are a little *kitchen-sinky* (see:
`man zshmisc`), most sub-systems are easy to locate.

### The Goal

Masterful shell usage comes with incremental effort born from the drive
to explore this fascinating working environment. This workshop intends
to provide the knowledge required to self-learn.


## Shell Types

* POSIX
    * pretty raw compared to bash
    * guaranteed to be portable
    * not guaranteed to be portable
    * Bash-like
        * designed for full-time use
        * bash, zsh, ???
* Others
    * rc/es - some kind of Plan 9 craziness
    * fish - remove the esoterica


## History

### Thompson Shell (sh)

Written by Ken Thompson, the `thompson shell` is a turning point in user
shells for computers and the DNA is clearly present in most shells built
after its introduction.

* pre-cursor was multics shell
    * first "shell" was RUNCOM (ctss) 1963 - '65
        * Louis Pouzin
            * MIT computer staff
            * coined the term 'shell'
        * RUNCOM => RUN COMmands
        * RUNCOM => rc files
    * first proper separation of user from kernel
* first release, 1971
* default shell until bourne shell in 1979
* not scritable
* main concepts:
    * redirection
        * clunky syntax in early versions
    * piping
        * douglas mcilroy
        * refined over time
    * globbing
        * `sh` relied on an external command to expand wildcard characters in unquoted arguments to a command
        * the command was `/etc/glob`
        * glob => global command
        * `glob()` now defined in POSIX
    * simple control structures
* informs all later shell design


### Post Thompson

|shell|year|notes|
|:----|:---|:----|
`sh`(bourne)|1977|Bell Labs, Stephen Bourne
`csh`|1978|BSD, Bill Joy, job control and a lot of bugs
`ksh`|1983|AT&T, David Korn, best of csh and sh (bourne), licensed$
`bash`|1989|GNU, Brian Fox
`rc`|1989|Bell Labs, Plan9, Tom Duff
`zsh`|1990|Princeton, Paul Falstad, named after Zhong Shao's login id
`es`|1993|Rakitzis and Haahr, child of rc, could be pretty hipster
`fish`|2005|Axel Liljencrantz


## Workshop Environment

We're going to use Docker and Voidlinux.

1. OSX users may need to set the key to the left of the spacebar to *Alt*
2. build the docker env: `docker build -t able .`
3. execute the zsh process: `docker run -it -h able able`


### A Note About Userland

The userland in this workshop will be GNU coreutils. Although
similar to BSD userland, some functionality is different so
some of these examples won't work on OS X or BSD (or any other
userland implementations). Use the manpages to suss the delta.


## Start-up Files (RC)

Read the manpage to find out the details; it's worth knowing some of the
specifics.

```
man zsh
```


### Check The RC File

```
cat .zshrc
```


## Builtins

```
man zshbuiltins
```

Let's take a quick look at some of ZSH's built in commands.

|builtin|purpose|
|:---|:---|
`.`|read commands from a *file* and execute in current shell
`alias`|define an alias
`bg`|put each specified job in the background or current job
`cd`|change the current directory
`echo`|write arguments to `stdout`; NOT PORTABLE
`fc`|control interactive history mechanism
`getopts`|check arguments for legal options
`jobs`|lists information about jobs
`kill`|sends either `SIGTERM` or the specified signal to a process or job
`print`|prints arguments to `stdout`
`printf`|prints arguments according to a format
`pwd`|print the absolute pathname of the current working dir.
`read`|read one line and break it into fields
`wait`|wait for the specified job or process

### Aliases

Aliasing often used command names is common practice; ZSH supports *global*
and *suffix* aliases also.

```
# use `vi` when a .txt filename is the command
alias -s txt=vi
somefile.txt

# global aliases can appear anywhere in a command line
alias -g Ig="|grep -i"
sort /etc/passwd Ig uu
```


## Parameters

```
man zshparam
```

In ZSH, a parameter is the name given to *variables*. They are set with the
`typeset` built-in or as `name=value`. Types are assigned to `name`s via an
attribute system - although types are very soft in shell environments, typing
is used for reasons other than operational enforcement.

### Types

* scalar (string, int, float)
* arrays (indexed, associative)
* exports
* special

ZSH has literal syntax for some types.

### Shell Parameters

A selection of interesting parameters set by the shell:

|parameter|value|
|:---:|:---|
`!`|the PID of last background job started
`$`|the PID of this shell (not subshells though)
`?`|the exit code of the previous command
`#`|the number of positional parameters
`*`|an array of positional parameters

```
# yay docker
echo $$

# guards don't eat return codes
false || echo $?

# shell can get nice and loose
f(){echo $#};f dog easy fox

# really loose
sleep 5&echo $!
```

### Parameters Used by the Shell

A swath of parameters are used by the shell to control various things.
These can be set in an RC file like `.zshrc`, used interactively, or in
scripts.

Check the `zshparam` man page for other parameters the shell sets, those
it responds to, and how to manipulate your own parameters. Later, we'll
cover *modifiers* for globbing and history events - these can also be applied
to parameters.


## The Line Editor

```
man zshzle
```

### GNU Readline

Readline is a GNU library used in many GNU programs - the least of which
is Bash. Shout-out to Brian Fox again.

ZSH uses its own line editor - ZLE.

### Keymaps

ZLE binds keys to widgets in sets of *keymaps*. The default keymap is *main*
which in turn is linked to either *emacs* or *viins*.

If the *main* keymap is deleted, the *.safe* keymap will be used. This keymap
is intentionally uncomfortable to use.

We'll be using the *emacs* keymap in this workshop as it's the default keymap
for both ZSH and Bash. Apparently *vi* mode is good, but I can't get my head
around it.

### Widgets

A large list of widgets I like follows. These are all bound by default.

*Note: `M` refers to `meta` and is normally invoked by the `Alt` key.*

|widget|emacs|description|
|:-----|:---:|:----------|
|backward-char|`C-b`|move backward one character|
|forward-char|`C-f`|move forward one character|
|backward-word|`M-b`|move to the beginning of the previous word|
|forward-word|`M-f`|move to the beginning of the next word|
|beginning-of-line|`C-a`|move to the beginning of the line|
|end-of-line|`C-e`|move to the end of the line|
|down-line-or-history|`C-n`|move down a line in buffer or next event in history|
|up-line-or-history|`C-p`|move up a line in buffer or previous event in history|
|history-incremental-search-backward|`C-r`|search backward through history|
|history-incremental-search-forward|`C-s`|search forward through history|
|history-search-backward|`M-p`|search backward in history for a line beginning with the first word|
|history-search-forward|`M-n`|search forward in history for a line beginning with the first word|
|insert-last-word|`M-.`|insert the last word from the previous line at cursor position|
|backward-kill-word|`C-w`, `M-^H`|kill the word behind the cursor|
|kill-word|`M-d`|kill the current word|
|capitalize-word|`M-c`|capitalize the current word and move past it|
|down-case-word|`M-l`|convert the current word to all lowercase and move past it|
|up-case-word|`M-u`|convert the current word to all caps and move past it|
|kill-line|`C-k`|kill from the cursor to the end of the line|
|kill-whole-line|`C-u`|kill the current line|
|quote-line|`M-'`|quote the current line|
|yank|`C-y`|insert the contents of the kill buffer at the cursor position|
|expand-or-complete|`TAB`|attempt shell expansion on the current word or attempt completion|
|accept-line|`C-j`,`C-m`|finish editing the buffer; execute|
|clear-screen|`C-l`|clear the screen and redraw the prompt|
|push-line|`C-q`,`M-q`|push the current line onto the buffer stack and clear the buffer|
|send-break|`C-g`|abort the current editor function|
|run-help|`M-h`|push the buffer onto the buffer stack and execute `run-help cmd`|
|undo|`C-_`,`C-x-u`|incrementally undo the last text modification|
|what-cursor-position|`C-x-=`|print the character under the cursor|
|which-command|`M-?`|push the buffer onto the buffer stack and execute `which-command cmd`|

Let's take some of that for a drive:

```
# let's inspect the lusers  -  accept-line
cat /etc/passwd<C-j>

# just false and nologin shells  -  insert-last-word
egrep 'false|nologin' <M-.>

# that uppercase GECOS name is annoying
<M-p> | sed 's/U/u/g'<C-j>
sed -i <M-.> <M-.><M-.>

# save a piece for later
<M-p><M-p><M-p><M-'><C-a>echo <C-e> >foo<M-q>  # hang on, right dir?
ls<C-j>  # ok, cool
<C-j>

# look at a thing
ls /u/s/z/sc<tab><tab><C-j>
<M-p><M-b>-la <C-j>
cat <M-.>
```

Do remember that there are other widgets which are not bound by default. The
`zshzle` man page contains the full list of standard widgets as well as
details about defining your own.


## Filename Generation (Globbing)

```
man 7 glob
man zshexpn
man zshoptions
```

Globbing allows patterns to be expanded into file lists. Refer to the
`zshoptions` man page for options which affect the mechanics of this system.

### Wildcard Matching

Wildcard patterns contain `?`, `*`, or `[`. Globbing is the operation that
expands a wildcard pattern into a list of pathnames matching the pattern.

* `?` matches any single character
* `*` matches any string including empty
* `[...]` matches a character class

As we're touching on expansion for the first time, let's quickly shout-out
to *brace expansion*:

```
print -l able.{c,h,o}
echo {1..4}
echo {0..10..3}
cp baker{.c,}
```

### Glob Qualifiers

Glob patterns can make use of *qualifiers* in parenthesis after the pattern
to filter or sort the matches. A selection of these qualifiers follows:

|qual|effect|
|:---|:---|
`/`|directories
`.`|plain files
`@`|symbolic links
`=`|sockets
`p`|named pipes
`*`|executable plain files
`fSPEC`|files with access rights matching *SPEC*
`n`|sets option `numeric_glob_sort` for current pattern
`oC`|sorts results using *C*
`OC`|sorts results descending using *C*

Optional modifiers can follow qualifiers within parens. See the
[history modifiers](#modifiers) section.


### Notes

* Filenames that start with `.` are not matched with wildcards
    * the `globdots` option changes this behaviour
* POSIX specifies that syntactically incorrect patterns are not expanded
    * this affects empty lists (globs which return no matches)
    * the `nullglob` and `nomatch` options provide control

### Give it a Crack

#### Basics

```
ls
ls *.c
ls baker.?
ls ^baker.?  # <C-p><M-b>^
ls *.[^c]
ls *.[^oh]
ls [^ab]*
ls ^(baker|nom)*
ls *.*~(able|baker).[ch]
ls *<->
ls nomnom<3-30>
ls nomnom<20->
ls nomnom<-3>
```

#### Qualifiers

```
ls -d *(/)  # dirs
ls *(@)     # links
ls *(p)     # pipes
ls *(.)     # regular files
ls *(x)     # executables
ls *(f664)  # perm mode
ls *(.L+1)  # non-empty regular files
ls /etc/*(a-1)       # accessed within the last day
echo <M-.><bs>:t)    # echo file names only 
print -l /etc/*(oa)  # sort files by access
print -l /etc/*(n:u) # sort numerically and uppercase
ls -lah /var/cache/xbps/*(.Lm+1)
```


## The Directory Stack

ZSH maintains an internal directory stack which can be used to easily travel
between a number of directories.

The *standard* method for interacting with the stack is via:

* `popd` - pop a directory from the stack
* `pushd` - push a directory onto the stack
* `dirs` - view and set the stack

I prefer a more intuitive method for interacting with the directory stack:

```
# push to the stack automatically, invert +/-, no dupes
cd /etc
cd /usr
cd /usr/local
cd /var
cd /run
cd
dirs -v
cd -<tab>2<C-j>
```

It's best to just start using this in your day-to-day in order to get a
realistic sense of the power.


## Redirection

```
man zshmisc
```

The manipulation of file descriptors.

The three default file descriptors are:

* `0: stdin` - the standard input
* `1: stdout` - the standard output
* `2: stderr` - the standard error

```
ls -la /proc/self/fd/
```

Well behaved Unix programs will read from `stdin`, emit text output on
`stdout`, and direct errors (or side-channel information) to `stderr`. By
default, `stdout` can be *piped* to another command's `stdin`.

When ZSH (and other shells) execute programs, they may configure the program's
`stdin`, `stdout`, or `stderr` file descriptors. This, for the interested,
happens before the `fork()`ed child calls `execve()`.

Essentially: prior to the command being executed, any redirections on the
command line are processed and the appropriate changes made to the program's
`0`, `1`, and `2` file descriptors. Once complete, the `execve()` consumes
the child.

|operator|description|
|:------:|:----------|
`<word`|opens file *word* for reading as `stdin`
`<>word`|opens file *word* for reading and writing; will create
`>word`|opens file *word* for writing; will create [CLOBBER]
`>|word`|as with `>`, but doesn't respect CLOBBER
`>!word`|as above
`>>word`|opens file *word* for writing in append mode; will create [CLOBBER]
`>>|`|and `>>!` are equivalent to above
`<<[-]word`|input is read up to *word* or *EOF*; here-doc; `-` to strip leading tabs
`<<<word`|input is read from expanded string *word*; here-string
`<&num`|`stdin` is duplicated from *num*; `man 2 dup2`
`>&num`|`stdout` is duplicated from *num*
`<&-`|close `stdin`
`>&-`|close `stdout`
`&>`|redirect both `stdin` and `stdout`

Use explicit numbers as a prefix to change the default of either `0` or `1`:

```
1>fname 2>&1  # stdout=fname, stderr=stdout
2>&1 1>fname  # stderr=terminal-stdout, stdout=fname
```

### Piping

As discussed, the `stdout` of a command can be easily *piped* to the `stdin`
of another command: `cmd | cmd`

A pipe can be specified with either `|&` or `|` - the former pipes both
`stdout` and `stderr`.

The *value* of a pipeline is the value of the last command unless the pipeline
is preceeded with `!` (logical inverse).

Pipelines can be broken out into a coprocess that can communicate with the parent
shell bi-directionally using `>&p` and `<&p`.

```
# create a func which uses both stdout and stderr
e(){echo "hello";echo "world" >&2;}

# test it
e | rev
e |& rev
foo=$(e|rev)
echo $foo
```

Take note of the shell option `pipefail` - without this option, pipes return
the exit code for the last component only, regardless of whether other parts
of the pipline return an error (`>0`).

### Process Substitution

```
man zshexpn
```

The short version: replace *list* with a file or pipe for a command to use.

|form|result|
|:---|:-----|
`<(list)`|provides an *fd* or *pipe* attached to *list*'s `stdout`
`>(list)`|as above for `stdin`
`=(list)`|provides a file for reading and writing to/from *list*

```
# diff over ssh is possible
diff -u <(sort /etc/passwd) <(sort -r /etc/passwd)

# it's all files
echo <(echo george) =(echo how)
```


## History Expansion

```
man zshexpn
```

History expansion provides a means to reference words from previous command
lines on the current command line. Each command is added, subject to some
rules, to the history list before it is executed. Each previous command is
called a *history event* and is assigned an incrementing number.

By default, you can reference history events by using the *bang* or *pling*:
`!`. This character, set by the shell parameter `histchars`, may appear
anywhere on a command line. A literal `!` can be escaped with single quotes
or a backslash.

The fully qualified form is: `event designator[:word designator[:mod...]]`

### Event Designators

|form|description|
|:---:|:---------|
`!`|start history expansion
`!!`|refer to the previous command; by itself repeats previous command
`!n`|refer to command-line `n`
`!-n`|refer to the current command-line minus `n`
`!str`|refer to the most recent command starting with `str`
`!?str[?]`|refer to the most recent command containing `str`
`!#`|refer to the current command line typed so far
`!{...}`|insulate history references

### Word Designators

*Note: some word designators require a `:` prefix*

|desig|req. `:`|description|
|:---:|:---:|:---|
`0`|y|the first input word (command)
`n`|y|the `n`th argument
`^`||the first argument
`$`||the last argument
`%`||the word matched by the most recent `?str` match
`x-y`|y|range of words; `x` defaults to `0`
`*`||all the arguments
`x*`|y|abbrv. for `x-$`
`x-`|y|like `x*` but omitting word `$`

### Modifiers

One or more `modifiers` may be applied after the optional `word designator`
each preceeded by `:`.

Presented is a selection, refer to the manpage for more modifiers:

|mod|effect|
|:---:|:---|
`a`|turn a file name into an absolute path
`e`|remove all but the extension
`h`|remove a trailing pathname component; `dirname`
`l`|convert the words to lowercase
`p`|print, but do not execute
`r`|remove a file extension, leaving the root name
`s/l/r`|substitute `r` for `l` once; use `gs/l/r` for global
`t`|remove leading pathnames; `basename`
`u`|convert the words to uppercase

```
# link a file with its full path
cd
echo hello >xray
ln -s !$:a /etc
ls -la !$/*(@)

# change a file's extension
cat charlie.o
mv !$ !$:r.bak

# remove leading path from 1st argument
cp /u/s/d/p/b<tab> .
ls -la !^:t
```


## GNU coreutils

A whirlwind tour of *GNU coreutils* - I'm leaving out the more esoteric tools.

Note for *BSD*/*OSX* folks; *BSD* doesn't have a notion of *coreutils*, so work
out which bits are different or missing and adjust. Or install *coreutils*.

The key takeaway is that the following tools work much like a *standard library*
for the system. They are available on all standard Linux machines except those
which only provide a [BusyBox](https://busybox.net) userland.

The following table follows the section breakdowns from the
[official manual](https://www.gnu.org/software/coreutils/manual/coreutils.html).

|program|summary|
|:------|:------|
`cat`|concatenate and write files
`tac`|`cat` in reverse
`nl`|number lines
`od`|output in octal and other formats
`fmt`|reformat paragraph text
`head`|output the first parts of files
`tail`|output the last parts of files
`wc`|print newline, word, and byte counts
`sort`|sort text files
`shuf`|shuffle text files
`uniq`|uniquify text files
`comm`|compare two sorted files line by line
`cut`|print selected parts of files
`paste`|merge lines of files
`tr`|translate, squeeze, and/or delete characters
`expand`|convert tabs to spaces
`unexpand`|convert spaces to tabs
`ls`|list files
`cp`|copy files
`dd`|convert and copy a file
`mv`|move (rename) files
`rm`|remove files
`shred`|remove files more securely
`link`|hardlink via syscall
`ln`|make links between files
`mkdir`|make directories
`mkfifo`|make named pipes (FIFOs)
`mknod`|make block or character special files
`readlink`|print value of a symlink or canonical file name
`rmdir`|remove empty directories
`unlink`|remove files via syscall
`chown`|change file owner and group
`chgrp`|change group ownership
`chmod`|change access permissions
`touch`|change file timestamps
`df`|report filesystem disk space usage
`du`|estimate file space usage
`stat`|report file or filesystem status
`sync`|synchronize cached writes to storage
`truncate`|shrink or extend the size of a file
`echo`|print a line of text
`printf`|format and print data
`yes`|print a string until interrupted
`false`|do nothing unsuccessfully
`true`|do nothing successfully
`test`|check file types and compare values
`tee`|redirect output to multiple files or processes
`basename`|strip directory and suffix from a filename
`dirname`|strip last file name component
`mktemp`|create a temporary file or directory
`realpath`|print resolved file name
`pwd`|print working directory
`stty`|manipulate the terminal characteristics
`printenv`|print all or some environment variables
`id`|print user identity
`groups`|print group names user is in
`who`|print who is currently logged in
`date`|print or set system date and time
`arch`|print machine hardware name
`nproc`|print the number of processors
`uname`|print system information
`hostname`|print or set system name
`uptime`|print system up-time and load
`chroot`|run a command with a different root directory
`env`|run a command with a modified environment
`nice`|run a command with a modified niceness
`nohup`|run a command immune to hang-ups (SIGHUP)
`timeout`|run a command with a time limit
`kill`|send a signal to a process
`sleep`|delay for a specified time
`numfmt`|format numbers
`seq`|print numeric sequences

```
# build the following 1 pipe element at a time
cut -d: -f-5 /etc/passwd | tr : '\t' | nl | sort -r
```


## Util-Linux

It bears mentioning that most Linux environments also ship with the venerable
*util-linux* package which is maintained by the Linux Kernel team. Most
of the programs included relate to system interaction, but some are useful
for everyday activities also. The table below highlights a few noteworthy
inclusions:

|program|summary|
|:------|:------|
`cal`|display a calendar
`col`|filter reverse line feeds from input
`column`|columnate lists
`flock`|manage locks from shell scripts
`getopt`|parse command options (via GNU getopt(3))
`kill`|send a signal to a process
`more`|file perusal filter for crt viewing
`newgrp`|log in to a new group
`nsenter`|run programs with namespaces of other processes
`rev`|reverse lines characterwise
`unshare`|run program with some namespaces unshared from parent
`uuidgen`|create a new UUID value
`vipw`,`vigr`|edit password or group file
`whereis`|locate binary, source, and man page files

```
# easy user lists
# tip: pwck -s to sort /etc/passwd by uid (shadow package)
column -ts: /etc/passwd

# what binaries in util-linux
# extra credit: combine the two greps
xbps-query -f util-linux | grep 'bin/' | grep -v '[>]' | sort | column

# lock and block
flock /dev/shm/foo.lock sleep 4&; flock /dev/shm/foo.lock echo 'foo free!'
```


## Summary

That's a relatively quick overview of some ZSH internals. It bears repeating:
the manpages are thorough and well-written (for the most part) and cover
many small problem domains. There are systems which have not been explicitly
mentioned (or brushed over), the least of which is the completion system
(`man zshcompsys`). Take it upon yourself to browse the documentation and to
constantly challenge your way of interacting with the shell.


## Fin

Merciless.
