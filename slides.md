# Z Shell Workshop

We will:

* study some of the systems in ZSH
* do some exercises as a demonstration of powa

(ง •̀ω•́)ง✧   行くぞ!

***

# Shell Types

There are two (pretty much) shell types:
* POSIX
* Bash-like

<br>
* POSIX sh is for scripting or system rescue
<br>
* it's a shitty place to visit and you don't want to live there
<br>
* Bash is good
<br>
* but ZSH is better
<br>
* for some values of better  (⌒_⌒;)

***

# Pay Your Respects

The first proper shell was the *Thompson shell*.

* 1965 - RUNCOM, CTSS
* 1971 - Thompson shell (sh)
* 1977 - Bourne shell (sh)
* 1978 - C shell (csh, tcsh)
* 1983 - Korn shell (ksh)
* 1989 - Bourne Again shell (bash)
* 1989 - run commands shell (rc)
* 1990 - ZSH
* 1993 - extensible shell (es)
* 2005 - fish

***

# Workshop Environment

Start up the docker environment:

~~~
docker build -t able .
docker run -it -h able able
~~~

* OSX users: make sure your Alt (Meta) key works

***

# Some Ground Rules

* the userland is GNU coreutils + util-linux
* which is why we're using Docker
<br>
* the RC file for this workshop is very light
* which is why we're using Docker
<br>
* think of the shell as your language
* and userland as your standard library

(¬‿¬)

***

# Builtins

~~~
man zshbuiltins
~~~

* .
* alias
* bg/fg/wait
* cd
* echo/print/printf
* fc
* getopts
* jobs
* kill
* pwd
* read

***

# Aliases

* a normal alias only works for the command
* which is the first word on a command line
* zsh aliases support suffixes and globals

~~~
# use 'vi' when the command word ends in .txt
alias -s txt=vi
somefile.txt
# quit 'vi' with ':q!'

# global aliases can appear anywhere in the command line
alias -g Ig="|grep -i"
sort /etc/passwd Ig uu
~~~

***

# Parameters

~~~
man zshparam
~~~

* parameters are variables.
* set parameters using *name=value* or *typeset*.
* parameters are typed via attributes.
* e.g., the *-x* attribute marks a param for export.
* parameters go through an expansion phase: `man zshexpn`

***

# Parameter Types

~~~
man zshparam
~~~

* Scalar - string, int, float
* Array - indexed, associative
* Exports
* Special

***

# Shell Parameters

~~~
man zshparam
~~~

* the shell sets some parameters, here are some:

~~~
# ! returns the PID of the last command
sleep 3& echo $!

# $ is the PID of the shell (not subshells)
echo $$

# ? yields the exit code of the previous command
false || echo $?

# # gives the number of arguments passed into block or script
f(){echo $#};f dog easy fox
~~~

***

# Parameters Used by the Shell

~~~
man zshparam
~~~

* there are a number of parameters the shell uses
* they can be set in your rc files
* or used in scripts
* or interactively

~~~
echo $PATH
PATH=/root ls
typeset -x CDPATH=/etc
cd colors
cd
~~~

***

# The Line Editor

~~~
man zshzle
~~~

* bash uses readline (thanks brian fox)
* zsh uses zle
* zle supports keymaps
* keymaps are sets of bindings
* bindings map keys to widgets
* the default keymap is emacs
* if you're crazy, there's a vi one
* there are standard widgets
* and special widgets
* and user widgets

***

# Standard Widgets

* <C-j> == accept-line == <Enter>
* <M-p> == up-line-or-history == <Up>

~~~
# list bound widgets
bindkey

# a contrived example
cat /etc/passwd<C-j>
egrep 'false|nologin' <M-.>  # try <M-h> before executing
<M-p> | sed 's/U/u/g'<C-j>
sed -i <M-.> <M-.><M-.>

# ever need to stop and check pwd?
<M-p><M-p><M-p><M-'><C-a>echo <C-e> >foo<M-q>
ls<C-j>
<C-j>

# look at a thing
ls /u/s/z/sc/<tab><tab><C-j>
<M-p><M-b>-la <C-j>
cat <M-.>
~~~

***

# Globbing

~~~
man 7 glob
man zshexpn
man zshoptions
~~~

* a glob is a pattern
* globs expand to lists of 0 or more files
* options affect how "no matches" are handled

* normal patterns use wildcard matching
* ? matches one character
* \* matches zero or more characters
* [...] matches a character class

***

# Brace Expansion Sidebar

* brace expansion occurs between braces *{}*
* comma separated words
* or ranges of integers

~~~
print -l able.{c,h,o}
echo {1..4}
echo {0..10..3}
cp baker{.c,}
~~~

***

# Glob Qualifiers

* qualifiers may follow a glob pattern
* use parens after a glob pattern to contain a qualifier

***

# Glob Examples

~~~
ls
ls *.c
ls baker.?
ls ^baker.?  # <M-p><M-b>^<C-j>
ls *.[^oh]
ls [^ab]*
ls ^(baker|nom)*
ls *.*~(able|baker).[ch]
ls *<->
ls nomnom<3-30>
ls -d *(/)
ls *(@)
ls *(x)
ls *(.L+1)
ls /etc/*(a-1)
echo <M-.><backspace>:t)
print -l /etc/*(oa)
print -l /etc/*(n:u)
ls -lah /var/cache/xbps/*(.Lm+1)
~~~

***

# Directory Stack

* zsh maintains an internal dirstack
* to allow easy traversal of common dirs
* normally one uses *popd*, *pushd*, and *dirs*
* that feels too manual for my taste

***

# Easy Dirstack Use

* some options are enabled in the RC file
* to make this more automatic
* *AUTO_PUSHD*
* *PUSHD_MINUS*
* *PUSHD_IGNORE_DUPS*

~~~
cd /etc; cd /usr; cd local; cd /var; cd /run; cd
dirs -v
cd -<tab>2<C-j>
cd
~~~

***

# Redirection

~~~
man zshmisc
~~~

* redirection is about manipulating file descriptors

~~~
# there are three default file descriptors
# 0: stdin
# 1: stdout
# 2: stderr
ls -la /proc/self/fd/
~~~

* well behaved unix programs read text on stdin...
* emit text on stdout...
* and direct errors (or oob data) on stderr
* stdout can be piped to another process

***

# Redirection Operators

* "< word"    opens file *word* for reading as *stdin*
* "<> word"   opens file *word* for reading+writing
* "> word"    opens file *word* for writing
* "<&num"     makes *num* a copy of *stdin*
* ">&num"     makes *num* a copy of *stdout*
* "&>"        redirects both *stdin* and *stdout*
* "<<[-]word" read input until *word* or *EOF* (here-doc)
* "<<<param"  set *stdin* to here-string

~~~
sed 's|https://||' <<!
https://github.com/
https://buildkite.com/
https://amazon.com/
!
~~~

***

# Piping

* a program's stdout can be piped to another command
* the output arrives on the other program's stdin
* pipelines can be broken out into coprocs
* pipelines return the exit code of the last command

~~~
# a func that sends text out on both stdout and stderr
e(){echo "hello";echo "world" >&2;}

e | rev       # reverse lines characterwise, util-linux
e |& rev      # pipe both stdout and stderr
foo=$(e|rev)  # command substitution
echo $foo
rev <<<$foo   # here-string
~~~

***

# Process Substitution

~~~
man zshexpn
~~~

* replace a command list with a file or pipe
* "<(list)" to use *list*'s stdout as stdin
* ">(list)" to use *list*'s stdin
* "=(list)" to use a file for both stdin and stdout

~~~
# the subshells could be ssh commands
diff -u <(sort /etc/passwd) <(sort -r /etc/passwd)

# look under the hood
echo <(echo george) =(echo how)
~~~

***

# History Expansion

~~~
man zshexpn
~~~

* reference words from previous command lines
* all commands are added to a list before execution
* these are referred to as *history events*
* use *bang* or *pling* to access these events

The full form is:

~~~
event-designator[:word-designator[:mod...]]
~~~

***

# Event Designators

A small selection:
~~~
!    :: start history expansion
!!   :: refer to the previous command
!n   :: refer to command line n
!-n  :: refer to command line minus n
!str :: refer to the most recent command starting with str
~~~

***

# Word Designators

* some word designators require *:* as a prefix

~~~
0 :: the first input word (command)
n :: the n'th argument
^ :: the first argument
$ :: the last argument
* :: all arguments
~~~

***

# Modifiers

* modifiers are used in other places (globs and params)
* preceed each with a colon *:*

~~~
a     :: absolute path
e     :: get the file extension
h     :: remove trailing pathname (dirname)
l     :: convert to lowercase
p     :: print, do not execute
r     :: remove file extension
s/l/r :: subst r for l once; gs/l/r for global
t     :: remove leading pathnames (basename)
u     :: convert to uppercase
~~~

***

# History Expansion Examples

~~~
# link a file with full path
cd
echo "hello" >xray
ln -s !$:a /etc
ls -la !$/*(@)

# change an extension
cat charlie.o
mv !$ !$:r.old

# remove leading path from 1st argument
cp /u/s/d/p/b<tab> .
ls -la !^:t
~~~

***

# END OF TRANSMISSION

✺◟( • ω • )◞✺
