setopt interactive_comments
setopt extended_glob
setopt auto_pushd
setopt pushd_minus
setopt pushd_ignore_dups

autoload -U compinit
compinit
